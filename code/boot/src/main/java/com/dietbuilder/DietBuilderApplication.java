package com.dietbuilder;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = "com.dietbuilder")
public class DietBuilderApplication {

    public static void main(String[] args) {
        SpringApplication.run(DietBuilderApplication.class, args);
    }
}
