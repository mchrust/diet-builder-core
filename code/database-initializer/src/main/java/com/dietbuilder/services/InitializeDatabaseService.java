package com.dietbuilder.services;

import com.dietbuilder.daos.ComestibleProductDao;
import com.dietbuilder.daos.MealDao;
import com.dietbuilder.documents.comestibleProduct.ComestibleProduct;
import com.dietbuilder.documents.meal.Meal;
import com.dietbuilder.repositories.SequenceRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class InitializeDatabaseService {

    @Autowired
    private ComestibleProductDao comestibleProductDao;

    @Autowired
    private MealDao mealDao;

    @Autowired
    private SequenceRepository sequenceRepository;

    @Autowired
    private LoadComestibleProductsFromXlsx loadComestibleProductListfromXlsxFile;

    @Autowired
    private LoadMealsForXlsx loadMealsForXlsx;

    public void initializeComestibleProducts() {
        List<ComestibleProduct> comestibleProducts = this.loadComestibleProductListfromXlsxFile.loadFromXlsxFile();
        log.info("comestibleproducts table has been initialized");

        this.comestibleProductDao.saveAll(comestibleProducts);
    }

    public void initializeMeals() {
        List<Meal> meals = loadMealsForXlsx.loadFromXlsxFile();
        log.info("meals table has been initialized");
        this.mealDao.saveAll(meals);
    }

    public void dropDatabase() {
        this.mealDao.deleteAll();
        this.comestibleProductDao.deleteAll();
        this.sequenceRepository.deleteAll();
        log.info("Database has been dropped");
    }
}
