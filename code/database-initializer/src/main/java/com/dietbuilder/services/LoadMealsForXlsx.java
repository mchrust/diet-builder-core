package com.dietbuilder.services;

import com.dietbuilder.config.InitializationProperties;
import com.dietbuilder.daos.MealDao;
import com.dietbuilder.documents.meal.Ingredient;
import com.dietbuilder.documents.meal.Meal;
import com.dietbuilder.documents.meal.IngredientUnit;
import com.dietbuilder.documents.meal.MealType;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Slf4j
public class LoadMealsForXlsx {

    @Autowired
    private InitializationProperties initializationProperties;

    private String rootDir;

    @Autowired
    private MealDao mealDao;

    @Autowired
    private SequenceGeneratorService sequenceGeneratorService;

    @PostConstruct
    private void setRootDir() {
        this.rootDir = System.getProperty("user.dir");
    }

    public List<Meal> loadFromXlsxFile() {
        ArrayList<Meal> meals = new ArrayList<>();
        try (FileInputStream fis = new FileInputStream(rootDir + initializationProperties.getFilePath())) {
            XSSFSheet sheet = new XSSFWorkbook(fis)
                    .getSheetAt(1);

            // removing header
            XSSFRow rowToRemove = sheet.getRow(0);
            sheet.removeRow(rowToRemove);

            // iterating through all xlsx rows
            Iterator<Row> rowIterator = sheet.rowIterator();
            log.info("Loading meals from xlsx file" + initializationProperties.getFilePath());

            XSSFRow mealRow = null;
            List<XSSFRow> ingredientsRows = null;
            XSSFRow row;
            while (rowIterator.hasNext()) {

                row = (XSSFRow) rowIterator.next();

                //check if this is row with basic meal infos, or with concrete ingredient data
                if ((Optional.ofNullable(row.getCell(0)).isPresent())) {
                    // check if this is first iteration, if now, all
                    if (mealRow != null) {
                        createMeal(meals, mealRow, ingredientsRows);
                    }

                    mealRow = row;
                    ingredientsRows = new ArrayList<>();
                } else {
                    assert ingredientsRows != null : "Ingredients row list have to be initialized";
                    ingredientsRows.add(row);
                }
            }
            assert ingredientsRows != null : "Ingredients row list have to be initialized";
            createMeal(meals, mealRow, ingredientsRows);
        } catch (IOException e) {
            log.error("Loading meals from xlsx file has failed", e);
        }

        return meals;
    }

    private void createMeal(List<Meal> meals, XSSFRow mealRow, List<XSSFRow> ingredientsRow) {
        List<Ingredient> ingredients = ingredientsRow.stream().map(this::loadMealIngredientFromXlsxRow).collect(Collectors.toList());
        Meal meal = loadMealHeaderFromXlsxRow(mealRow, ingredients);
        meals.add(meal);
    }

    /**
     * @return Meal
     * Method pull mealName, description and recipe from row and save it a Meal entity.
     */
    private Meal loadMealHeaderFromXlsxRow(XSSFRow row, List<Ingredient> ingredients) {
        return Meal.builder()
                .id(sequenceGeneratorService.generateSequence(Meal.SEQUENCE_NAME))
                .mealName(row.getCell(0).toString())
                .description(row.getCell(1).toString())
                .recipe(row.getCell(2).toString())
                .mealType(MealType.valueOf(row.getCell(3).toString().toUpperCase()))
                .ingredients(ingredients)
                .build();
    }

    /**
     * @return Meal
     * Method pull ingredient from row and save it a Meal entity.
     */
    private Ingredient loadMealIngredientFromXlsxRow(XSSFRow row) {
        return Ingredient.builder()
                .comestibleProductID((long) row.getCell(4).getNumericCellValue())
                .amount(row.getCell(5).getNumericCellValue())
                .ingredientUnit(IngredientUnit.valueOf(row.getCell(6).getStringCellValue().toUpperCase()))
                .build();
    }
}
