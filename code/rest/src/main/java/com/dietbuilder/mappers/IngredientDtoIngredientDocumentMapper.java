package com.dietbuilder.mappers;

import com.dietbuilder.daos.ComestibleProductDao;
import com.dietbuilder.documents.meal.Ingredient;
import com.dietbuilder.documents.meal.IngredientUnit;
import com.dietbuilder.documents.meal.Meal;
import com.dietbuilder.model.IngredientDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Component
public class IngredientDtoIngredientDocumentMapper {

    @Autowired
    private ComestibleProductDao comestibleProductDao;

    @Autowired
    private ComestibleProductDtoComestibleProductDocumentMapper comestibleProductMapper;

    public Ingredient sourceToDestination(IngredientDto source) {
        return Ingredient.builder()
                .comestibleProductID(source.getComestibleProduct().getId())
                .amount(source.getAmount())
                .ingredientUnit(IngredientUnit.valueOf(source.getIngredientUnit().getValue()))
                .build();
    }

    public List<Ingredient> sourceToDestination(List<IngredientDto> source) {
        return source.stream().map(this::sourceToDestination).collect(Collectors.toList());
    }

    public IngredientDto destinationToSource(Ingredient destination) {
        IngredientDto ingredientDto = new IngredientDto();
        ingredientDto
                .comestibleProduct(comestibleProductMapper.destinationToSource(comestibleProductDao.get(destination.getComestibleProductID())))
                .amount(destination.getAmount())
                .ingredientUnit(IngredientDto.IngredientUnitEnum.valueOf(destination.getIngredientUnit().toString()));
        return ingredientDto;
    }

    public List<IngredientDto> destinationToSource(List<Ingredient> destination) {
        return destination.stream().map(this::destinationToSource).collect(Collectors.toList());
    }

    public List<Ingredient> updateIngredients(Meal currentMeal, Meal newMeal) {
        List<Ingredient> newIngredients = newMeal.getIngredients();
        List<Ingredient> currentIngredients = currentMeal.getIngredients();

        for (int i = 0; i < newIngredients.size(); i++) {
            boolean ingredientAlreadyPresentInMeal = false;

            Ingredient newIngredient = newIngredients.get(i);

            for (int j = 0; j < currentIngredients.size(); j++) {
                if (Objects.equals(currentIngredients.get(j).getComestibleProductID(), newIngredient.getComestibleProductID())) {
                    currentIngredients.set(j, updateIngredient(currentIngredients.get(j), newIngredient));

                    ingredientAlreadyPresentInMeal = true;
                    break;
                }
            }

            // add new ingredient if already do not exist in list
            if (!ingredientAlreadyPresentInMeal) {
                currentIngredients.add(newIngredient);
            }
        }
        return currentIngredients;
    }

    public Ingredient updateIngredient(Ingredient currentIngredient, Ingredient newIngredient) {
        return Ingredient.builder()
                .comestibleProductID(!Objects.isNull(newIngredient.getComestibleProductID()) ? newIngredient.getComestibleProductID() : currentIngredient.getComestibleProductID())
                .ingredientUnit(!Objects.isNull(newIngredient.getIngredientUnit()) ? newIngredient.getIngredientUnit() : currentIngredient.getIngredientUnit())
                .amount(!Objects.isNull(newIngredient.getAmount()) ? newIngredient.getAmount() : currentIngredient.getAmount())
                .build();
    }
}
