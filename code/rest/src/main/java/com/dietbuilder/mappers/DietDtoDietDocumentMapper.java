package com.dietbuilder.mappers;

import com.dietbuilder.documents.diet.Diet;
import com.dietbuilder.documents.diet.DietStatus;
import com.dietbuilder.model.DietDto;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Component
public class DietDtoDietDocumentMapper {

    public Diet sourceToDestination(DietDto source) {
        return Diet.builder()
                .id(source.getId())
                .dietStatus(DietStatus.valueOf(source.getDietStatus().getValue()))
                .firstMealsList(source.getFirstMealsList())
                .secondMealsList(source.getSecondMealsList())
                .thirdMealsList(source.getThirdMealsList())
                .fourthMealsList(Objects.nonNull(source.getFourthMealsList()) ? source.getFourthMealsList() : null)
                .fifthMealsList(Objects.nonNull(source.getFifthMealsList()) ? source.getFifthMealsList() : null)
                .sixthMealsList(Objects.nonNull(source.getSixthMealsList()) ? source.getSixthMealsList() : null)
                .seventhMealsList(Objects.nonNull(source.getSeventhMealsList()) ? source.getSeventhMealsList() : null)
                .build();
    }

    public DietDto destinationToSource(Diet destination) {
        return new DietDto()
                .id(destination.getId())
                .dietStatus(DietDto.DietStatusEnum.valueOf(destination.getDietStatus().toString()))
                .firstMealsList(destination.getFirstMealsList())
                .secondMealsList(destination.getSecondMealsList())
                .thirdMealsList(destination.getThirdMealsList())
                .fourthMealsList(Objects.nonNull(destination.getFourthMealsList()) ? destination.getFourthMealsList() : null)
                .fifthMealsList(Objects.nonNull(destination.getFifthMealsList())? destination.getFifthMealsList() : null)
                .sixthMealsList(Objects.nonNull(destination.getSixthMealsList())? destination.getSixthMealsList() : null)
                .seventhMealsList(Objects.nonNull(destination.getSeventhMealsList())? destination.getSeventhMealsList() : null);
    }
}
