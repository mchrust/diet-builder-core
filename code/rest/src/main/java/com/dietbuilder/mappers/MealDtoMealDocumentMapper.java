package com.dietbuilder.mappers;

import com.dietbuilder.daos.MealDao;
import com.dietbuilder.documents.meal.Meal;
import com.dietbuilder.documents.meal.MealType;
import com.dietbuilder.exceptions.MealNotFoundException;
import com.dietbuilder.model.MealDto;
import com.dietbuilder.services.SequenceGeneratorService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Objects;

@Component
@Slf4j
public class MealDtoMealDocumentMapper {

    @Autowired
    private MealDao mealDao;

    @Autowired
    private IngredientDtoIngredientDocumentMapper ingredientMapper;

    @Autowired
    SequenceGeneratorService sequenceGeneratorService;

    public Meal sourceToDestination(MealDto source) {
        return Meal.builder()
                .id(source.getId() != null ? source.getId() : sequenceGeneratorService.generateSequence(Meal.SEQUENCE_NAME))
                .ingredients(Objects.nonNull(source.getIngredients()) ? ingredientMapper.sourceToDestination(source.getIngredients()) : new ArrayList<>())
                .mealName(source.getMealName())
                .description(source.getDescription())
                .recipe(source.getRecipe())
                .mealType(MealType.valueOf(source.getMealType().toString()))
                .build();
    }


    public MealDto destinationToSource(Meal destination) {
        MealDto mealDto = new MealDto();
        return mealDto
                .id(destination.getId())
                .ingredients(ingredientMapper.destinationToSource(destination.getIngredients()))
                .mealName(destination.getMealName())
                .description(destination.getDescription())
                .recipe(destination.getRecipe())
                .mealType(Objects.nonNull(destination.getMealType()) ? MealDto.MealTypeEnum.valueOf(destination.getMealType().toString()) : null);
    }

    public Meal updateMeal(Meal givenMeal) {
        Meal currentMeal;
        try {
            currentMeal = mealDao.get(givenMeal.getId());
        } catch (MealNotFoundException e) {
            log.info("Meal with this ID does not exist. Creating new one.");
            return givenMeal;
        }

        return Meal.builder()
                .id(givenMeal.getId())
                .ingredients(!givenMeal.getIngredients().isEmpty() ? ingredientMapper.updateIngredients(currentMeal, givenMeal) : currentMeal.getIngredients())
                .mealName(givenMeal.getMealName() != null ? givenMeal.getMealName() : currentMeal.getMealName())
                .description(givenMeal.getDescription() != null ? givenMeal.getDescription() : currentMeal.getDescription())
                .recipe(givenMeal.getRecipe() != null ? givenMeal.getRecipe() : currentMeal.getRecipe())
                .mealType(givenMeal.getMealType() != null ? givenMeal.getMealType() : currentMeal.getMealType())
                .build();

    }

}
