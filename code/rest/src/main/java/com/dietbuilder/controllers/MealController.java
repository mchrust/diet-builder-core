package com.dietbuilder.controllers;

import com.dietbuilder.api.MealsApi;
import com.dietbuilder.daos.MealDao;
import com.dietbuilder.documents.meal.Meal;
import com.dietbuilder.exceptions.MealNotFoundException;
import com.dietbuilder.mappers.MealDtoMealDocumentMapper;
import com.dietbuilder.model.MealDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@Slf4j
public class MealController implements MealsApi {

    @Autowired
    private MealDao mealDao;

    @Autowired
    private MealDtoMealDocumentMapper mealDTOMealDocumentMapper;

    @Override
    public ResponseEntity<MealDto> getMealById(@PathVariable Long id) {
        Meal meal;
        try {
            meal = mealDao.get(id);
        } catch (MealNotFoundException e) {
            log.error("Meal with this ID does not exist.");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return ResponseEntity.ok(mealDTOMealDocumentMapper.destinationToSource(meal));
    }

    @Override
    public ResponseEntity<List<MealDto>> getMealsByName(@PathVariable String mealName) {
        List<Meal> meals = mealDao.getAllByName(mealName);

        List<MealDto> result = meals.stream().map(meal -> mealDTOMealDocumentMapper.destinationToSource(meal)).collect(Collectors.toList());
        return ResponseEntity.ok(result);
    }

    @Override
    public ResponseEntity<List<MealDto>> getAllMeals() {
        List<Meal> meals = mealDao.getAll();
        List<MealDto> result = meals.stream().map(m -> mealDTOMealDocumentMapper.destinationToSource(m)).collect(Collectors.toList());
        return ResponseEntity.ok(result);
    }

    @Override
    public ResponseEntity<Void> insertMeal(@RequestBody MealDto meal) {

        try {
            mealDao.insert(mealDTOMealDocumentMapper.sourceToDestination(meal));
        } catch (DuplicateKeyException e) {
            log.error("Meal cannot be saved.", e);
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Void> updateMeal(@RequestBody MealDto meal) {
        Meal mealToSave = mealDTOMealDocumentMapper.updateMeal(mealDTOMealDocumentMapper.sourceToDestination(meal));
        mealDao.save(mealToSave);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Void> deleteMeal(@RequestBody MealDto meal) {
        mealDao.delete(Meal.builder().id(meal.getId()).build());
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Void> deleteMealById(@PathVariable Long id) {
        mealDao.deleteById(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
