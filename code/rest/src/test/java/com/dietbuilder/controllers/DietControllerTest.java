package com.dietbuilder.controllers;

import com.dietbuilder.daos.DietDao;
import com.dietbuilder.daos.UserDao;
import com.dietbuilder.documents.diet.Diet;
import com.dietbuilder.documents.diet.DietStatus;
import com.dietbuilder.documents.user.Gender;
import com.dietbuilder.documents.user.User;
import com.dietbuilder.documents.user.UserGoal;
import com.dietbuilder.mappers.DietDtoDietDocumentMapper;
import com.dietbuilder.repositories.DietRepository;
import com.dietbuilder.repositories.UserRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Optional;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(classes = {DietController.class, DietDtoDietDocumentMapper.class, DietDao.class, UserDao.class})
@EnableWebMvc
@AutoConfigureMockMvc(addFilters = false)
class DietControllerTest {

    @MockBean
    private DietRepository dietRepository;

    @MockBean
    private UserRepository userRepository;

    @Autowired
    private MockMvc mvc;

    private final Long DIET_ID = 1L;

    private final Long USER_ID = 1L;

    @Test
    void givenDietId_whenProcessing_thenReturnsCorrectDiet() throws Exception {
        when(this.dietRepository.findById(DIET_ID)).thenReturn(Optional.of(sampleDiet()));

        final String URI = "/diets/byDietId/{dietId}";

        this.mvc.perform(get(URI, DIET_ID)
                        .accept(MediaType.APPLICATION_JSON)
                        .characterEncoding(StandardCharsets.UTF_8))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(DIET_ID))
                .andExpect(jsonPath("$.dietStatus").value("ACTIVE"))
                .andReturn();
    }

    @Test
    void givenUserId_whenProcessing_thenReturnsCorrectDiet() throws Exception {
        when(this.dietRepository.findById(DIET_ID)).thenReturn(Optional.of(sampleDiet()));
        when(this.userRepository.findById(USER_ID)).thenReturn(Optional.of(sampleUser()));

        final String URI = "/diets/byUserId/{userId}";

        this.mvc.perform(get(URI, DIET_ID)
                        .accept(MediaType.APPLICATION_JSON)
                        .characterEncoding(StandardCharsets.UTF_8))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(DIET_ID))
                .andExpect(jsonPath("$.dietStatus").value("ACTIVE"))
                .andReturn();
    }

    private Diet sampleDiet() {
        return Diet.builder()
                .id(DIET_ID)
                .dietStatus(DietStatus.ACTIVE)
                .firstMealsList(new ArrayList<>())
                .secondMealsList(new ArrayList<>())
                .thirdMealsList(new ArrayList<>())
                .fourthMealsList(new ArrayList<>())
                .fifthMealsList(new ArrayList<>())
                .build();
    }

    private User sampleUser() {
        return User.builder()
                .userId(USER_ID)
                .dietId(DIET_ID)
                .build();
    }

}
