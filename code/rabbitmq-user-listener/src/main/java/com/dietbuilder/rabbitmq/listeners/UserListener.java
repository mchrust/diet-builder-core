package com.dietbuilder.rabbitmq.listeners;

import com.dietbuilder.rabbitmq.mappers.UserDataToUserModelMapper;
import com.dietbuilder.rabbitmq.model.User;
import com.dietbuilder.services.DietService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@AllArgsConstructor
public class UserListener {

    private final DietService dietService;

    private final UserDataToUserModelMapper mapper;

    @RabbitListener(queues = "${spring.rabbitmq.queue}")
    public void receiveUser(User user) {
        log.info("Starting processing user with id: " + user.getId());
        this.dietService.createDiet(mapper.sourceToDestination(user));

    }

}
