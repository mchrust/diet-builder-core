package com.dietbuilder.documents.diet;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Document(collection = "diets")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@EqualsAndHashCode
@Builder(toBuilder = true)
public class Diet {

    @Transient
    public static final String SEQUENCE_NAME = "diet_sequence";

    @Id
    private Long id;

    private DietStatus dietStatus;

    private List<Long> firstMealsList;

    private List<Long> secondMealsList;

    private List<Long> thirdMealsList;

    private List<Long> fourthMealsList;

    private List<Long> fifthMealsList;

    private List<Long> sixthMealsList;

    private List<Long> seventhMealsList;
}
