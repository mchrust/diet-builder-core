package com.dietbuilder.documents.meal;

public enum MealType {
  BREAKFAST, BRUNCH, DINNER, TEA, SUPPER
}
