package com.dietbuilder.documents.meal;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;

@Document(collection = "meals")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@EqualsAndHashCode
@Builder
public class Meal {

    @Transient
    public static final String SEQUENCE_NAME = "meals_sequence";

    @Id
    private Long id;

    @Singular
    private List<Ingredient> ingredients = new ArrayList<>();

    @Indexed(unique = true)
    private String mealName;

    private String description;

    private String recipe;

    private MealType mealType;

    private Integer calories;
}
