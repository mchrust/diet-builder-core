package com.dietbuilder.documents.diet;

public enum DietStatus {
    IN_CREATION, ACTIVE, INACTIVE
}
