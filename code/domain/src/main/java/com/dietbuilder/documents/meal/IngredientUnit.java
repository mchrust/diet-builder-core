package com.dietbuilder.documents.meal;

public enum IngredientUnit {
  G, ML, UNIT, SPOON
}
