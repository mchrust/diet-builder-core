package com.dietbuilder.documents.meal;


import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode
@Builder
public class Ingredient {

    private Long comestibleProductID;

    private Double amount;

    private IngredientUnit ingredientUnit;
}
