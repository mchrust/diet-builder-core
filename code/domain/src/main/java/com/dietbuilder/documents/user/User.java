package com.dietbuilder.documents.user;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Document(collection = "users")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@EqualsAndHashCode
@Builder(toBuilder = true)
public class User {

    @Transient
    public static final String SEQUENCE_NAME = "user_sequence";

    @Id
    private Long userId;

    private Gender gender;

    private Double height;

    private Double weight;

    private Integer age;

    private Double psychicalActivityRatio;

    private Double basalEnergyExpenditure;

    private Double totalEnergyExpenditure;

    private Integer numberOfMeals;

    private UserGoal userGoal;

    private Long dietId;

    private List<Long> unwantedComestibleProductsIDs;
}
