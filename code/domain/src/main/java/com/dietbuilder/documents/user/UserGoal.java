package com.dietbuilder.documents.user;

public enum UserGoal {
    REDUCE,
    MAINTAIN,
    CARRY
}
