package com.dietbuilder.documents.user;

public enum Gender {
    MALE, FEMALE
}
