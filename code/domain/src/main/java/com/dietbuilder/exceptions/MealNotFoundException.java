package com.dietbuilder.exceptions;

import java.util.NoSuchElementException;

public class MealNotFoundException extends NoSuchElementException {
    public MealNotFoundException(String s) {
        super(s);
    }
}
