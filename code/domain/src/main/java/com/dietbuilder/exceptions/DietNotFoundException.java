package com.dietbuilder.exceptions;

import java.util.NoSuchElementException;

public class DietNotFoundException extends NoSuchElementException {
    public DietNotFoundException(String s) {
        super(s);
    }
}
