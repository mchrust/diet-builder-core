package com.dietbuilder.repositories;

import com.dietbuilder.documents.databaseSequence.DatabaseSequence;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SequenceRepository extends MongoRepository<DatabaseSequence, String> {
    void deleteAll();
}
