package com.dietbuilder.repositories;

import com.dietbuilder.documents.meal.Meal;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MealRepository extends MongoRepository<Meal, Long> {
  
  List<Meal> findAllByOrderByMealNameAsc();

  List<Meal> findByMealNameContainsIgnoreCase(String mealName);

  @Query(value = "{'calories': { $gte: ?0, $lte: ?1}," +
          "'mealType': ?2, "+
          "'ingredients.comestibleProductID': {$nin: ?3}}")
  List<Meal> findMealsByCaloriesAndMealTypeExcludingUnwantedIngredients(double minCalories, double maxCalories, String mealType, Long[] comestibleProductIds);

  void deleteAll();
}
