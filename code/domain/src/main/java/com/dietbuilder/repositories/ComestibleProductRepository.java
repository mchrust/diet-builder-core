package com.dietbuilder.repositories;

import com.dietbuilder.documents.comestibleProduct.ComestibleProduct;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ComestibleProductRepository extends MongoRepository<ComestibleProduct, Long> {
  List<ComestibleProduct> findByProductNameContainsIgnoreCase(String productName);

  @Query("{'productName':?0}")
  ComestibleProduct findComestibleProductsByName(String name);

  void deleteAll();
}
