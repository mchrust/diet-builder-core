package com.dietbuilder.repositories;

import com.dietbuilder.documents.diet.Diet;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DietRepository extends MongoRepository<Diet, Long> {

}
