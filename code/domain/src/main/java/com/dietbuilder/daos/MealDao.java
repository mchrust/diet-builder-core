package com.dietbuilder.daos;

import com.dietbuilder.documents.meal.Meal;
import com.dietbuilder.exceptions.MealNotFoundException;
import com.dietbuilder.repositories.MealRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
@Slf4j
public class MealDao {

  @Autowired
  private MealRepository mealRepository;

  public Meal get(long id) {
    Optional<Meal> mealOptional = mealRepository.findById(id);

    if (mealOptional.isPresent()){
      log.info("Meal with id: " + id + " has been found");
      return mealOptional.get();
    } else {
      throw new MealNotFoundException("Cannot find Meal with id: " + id);
    }
  }

  public List<Meal> getAllByName(String mealName) {
    List<Meal> meals = mealRepository.findByMealNameContainsIgnoreCase(mealName);
    log.info("Meals list has been found");
    return meals;
  }

  public List<Meal> getAll() {
    List<Meal> meals = mealRepository.findAllByOrderByMealNameAsc();
    log.info("Meals list has been found");
    return meals;
  }

  public void insert(Meal meal) {
    mealRepository.insert(meal);
    log.info("Meal has been inserted");
  }

  public void save(Meal meal) {
    mealRepository.save(meal);
    log.info("Meal has been saved");
  }

  public void saveAll(List<Meal> meals) {
    meals.forEach(this::save);
    log.info("Meals has been saved");
  }

  public void delete(Meal meal) {
    mealRepository.delete(meal);
    log.info("Meal has been deleted");
  }

  public void deleteById(long id) {
    mealRepository.deleteById(id);
    log.info("Meal has been deleted");
  }

  public void deleteAll() {
    mealRepository.deleteAll();
    log.info("All meals have been deleted");
  }
}
