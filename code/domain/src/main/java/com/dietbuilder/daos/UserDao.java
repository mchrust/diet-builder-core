package com.dietbuilder.daos;

import com.dietbuilder.documents.user.User;
import com.dietbuilder.exceptions.UserNotFoundException;
import com.dietbuilder.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class UserDao  {

    @Autowired
    private UserRepository userRepository;

    public User getUserById(Long id) {
        Optional<User> userOptional = userRepository.findById(id);

        if (userOptional.isPresent()) {
            return userOptional.get();
        } else {
            throw new UserNotFoundException("Cannot find user with id: " + id);
        }
    }

}
