package com.dietbuilder.daos;

import com.dietbuilder.documents.diet.Diet;
import com.dietbuilder.documents.user.User;
import com.dietbuilder.exceptions.DietNotFoundException;
import com.dietbuilder.repositories.DietRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Objects;
import java.util.Optional;

@Component
@Slf4j
public class DietDao {

    @Autowired
    private DietRepository dietRepository;

    @Autowired
    private UserDao userDao;



    public Diet findDietByDietId(Long id) {
        Optional<Diet> dietOptional = dietRepository.findById(id);

        if (dietOptional.isPresent()) {
            log.info("Diet has been found");
            return dietOptional.get();
        } else {
            throw new DietNotFoundException("Cannot find diet with id: " + id);
        }
    }

    public Diet findDietByUserId(Long id) {
        User user = userDao.getUserById(id);
        
        Long dietId = user.getDietId();

        Optional<Diet> dietOptional = null;
        
        if (Objects.nonNull(dietId)) {
            dietOptional = dietRepository.findById(dietId);
        }

        if (dietOptional.isPresent()) {
            log.info("Diet has been found");
            return dietOptional.get();
        } else {
            throw new DietNotFoundException("Cannot find diet with id: " + id);
        }
    }
}
