package com.dietbuilder.model.dayDetail;

import lombok.Data;

@Data
public class MealDetail {

    private String startHour;
    private String endHour;
    private int lowestAmountOfCalories;
    private int greatestAmountOfCalories;
    private MealType mealType;

    public MealDetail(String startHour, String endHour, int lowestAmountOfCalories, int greatestAmountOfCalories, MealType mealType) {
        this.startHour = startHour;
        this.endHour = endHour;
        this.lowestAmountOfCalories = lowestAmountOfCalories;
        this.greatestAmountOfCalories = greatestAmountOfCalories;
        this.mealType = mealType;
    }
}
