package com.dietbuilder.model.dayDetail;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public enum MealsInDayPolicy {
    THREE(3, Collections.emptyList()),
    FOUR(4, Collections.emptyList()),
    FIVE(5,
            new ArrayList<>(Arrays.asList(
                    new MealDetail("8", "10", 20, 25, MealType.BREAKFAST),
                    new MealDetail("11", "13", 15, 20, MealType.BRUNCH),
                    new MealDetail("14", "16", 30, 35, MealType.DINNER),
                    new MealDetail("17", "18", 5, 10, MealType.TEA),
                    new MealDetail("19", "20", 20, 25, MealType.SUPPER))
            )),
    SIX(6, Collections.emptyList());

    public final int numberOfMeals;

    public final List<MealDetail> mealDetail;

    MealsInDayPolicy(int numberOfMeals, List<MealDetail> mealDetails) {
        this.numberOfMeals = numberOfMeals;
        this.mealDetail = mealDetails;
    }

}
