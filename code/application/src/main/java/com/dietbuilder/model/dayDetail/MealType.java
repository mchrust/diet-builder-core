package com.dietbuilder.model.dayDetail;

public enum MealType {
  BREAKFAST, BRUNCH, DINNER, TEA, SUPPER
}
