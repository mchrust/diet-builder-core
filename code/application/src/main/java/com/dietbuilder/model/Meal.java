package com.dietbuilder.model;

import com.dietbuilder.documents.meal.MealType;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.ArrayList;
import java.util.List;

@Data
@EqualsAndHashCode
public class Meal {

    private List<Ingredient> ingredients = new ArrayList<>();

    private String mealName;

    private String description;

    private String recipe;

    private MealType mealType;
}
