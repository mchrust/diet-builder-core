package com.dietbuilder.strategies;

import com.dietbuilder.documents.diet.Diet;
import com.dietbuilder.documents.user.User;
import com.dietbuilder.strategies.strategyName.GenerateDietStrategyName;

public interface GenerateDietStrategy {
    Diet generateDiet(User user);
    GenerateDietStrategyName getStrategy();
}
