package com.dietbuilder.strategies;

import com.dietbuilder.documents.user.User;
import com.dietbuilder.strategies.strategyName.UserIndicatorGenerateStrategyName;

public interface UserIndicatorsGenerateStrategy {
    User generateIndicators(User user);
    UserIndicatorGenerateStrategyName getStrategy();
}
