package com.dietbuilder.strategies.impl;

import com.dietbuilder.documents.diet.Diet;
import com.dietbuilder.documents.diet.DietStatus;
import com.dietbuilder.documents.meal.Meal;
import com.dietbuilder.documents.user.User;
import com.dietbuilder.model.dayDetail.MealDetail;
import com.dietbuilder.model.dayDetail.MealsInDayPolicy;
import com.dietbuilder.repositories.DietRepository;
import com.dietbuilder.repositories.MealRepository;
import com.dietbuilder.services.SequenceGeneratorService;
import com.dietbuilder.strategies.GenerateDietStrategy;
import com.dietbuilder.strategies.strategyName.GenerateDietStrategyName;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Component
@Slf4j
public class HealthYoungHumanDietGenerateStrategy implements GenerateDietStrategy {

    @Autowired
    private MealRepository mealRepository;

    @Autowired
    private DietRepository dietRepository;

    @Autowired
    private SequenceGeneratorService sequenceGeneratorService;

    @Override
    public Diet generateDiet(User user) {
        Diet diet = Diet.builder().id(sequenceGeneratorService.generateSequence(Diet.SEQUENCE_NAME))
                .dietStatus(DietStatus.IN_CREATION).build();

        dietRepository.save(diet);

        double caloriesPerDay = new CaloriesCalculator().calculateAmountOfCaloriesPerDay(user);

        MealsInDayPolicy mealsInDayPolicy = Arrays.stream(MealsInDayPolicy.values())
                .filter(policy -> policy.numberOfMeals == user.getNumberOfMeals())
                .findAny()
                .orElseThrow();

        Long[] unwantedComestibleProductIDs = new Long[user.getUnwantedComestibleProductsIDs().size()];
        user.getUnwantedComestibleProductsIDs().toArray(unwantedComestibleProductIDs);

        for (int i = 0; i < mealsInDayPolicy.numberOfMeals; i++) {
            diet = findMeals(diet, caloriesPerDay, mealsInDayPolicy.mealDetail.get(i), i, unwantedComestibleProductIDs);
        }

        diet = diet.toBuilder().dietStatus(DietStatus.ACTIVE).build();

        dietRepository.save(diet);

        log.info("Diet has been created and saved.");

        return diet;
    }

    private Diet findMeals(Diet diet, double caloriesPerDay, MealDetail mealDetail, int mealNumber, Long[] unwantedComestibleProductsIDs) {
        double lowestAmountOfCalories = caloriesPerDay * mealDetail.getLowestAmountOfCalories();
        double greatestAmountOfCalories = caloriesPerDay * mealDetail.getGreatestAmountOfCalories();

        List<com.dietbuilder.documents.meal.Meal> meals = mealRepository.findMealsByCaloriesAndMealTypeExcludingUnwantedIngredients(lowestAmountOfCalories,
                greatestAmountOfCalories,
                mealDetail.getMealType().toString(),
                unwantedComestibleProductsIDs);

        List<Long> mealsIDs = meals.stream().map(Meal::getId).collect(Collectors.toList());

        switch (mealNumber) {
            case 0:
                diet = diet.toBuilder().firstMealsList(mealsIDs).build();
                break;
            case 1:
                diet = diet.toBuilder().secondMealsList(mealsIDs).build();
                break;
            case 2:
                diet = diet.toBuilder().thirdMealsList(mealsIDs).build();
                break;
            case 3:
                diet = diet.toBuilder().fourthMealsList(mealsIDs).build();
                break;
            case 4:
                diet = diet.toBuilder().fifthMealsList(mealsIDs).build();
                break;
            case 5:
                diet = diet.toBuilder().sixthMealsList(mealsIDs).build();
                break;
            case 6:
                diet = diet.toBuilder().seventhMealsList(mealsIDs).build();
                break;
        }
        return diet;
    }


    static class CaloriesCalculator {
        private double calculateAmountOfCaloriesPerDay(User user) {
            if (Objects.isNull(user.getUserGoal())) {
                throw new IllegalStateException("User goal cannot be null");
            }

            switch (user.getUserGoal()) {
                case REDUCE:
                    return calculateAmountOfCaloriesPerDayWhenReducting(user);
                case CARRY:
                    return calculateAmountOfCaloriesPerDayWhenCarrying(user);
                case MAINTAIN:
                    return user.getTotalEnergyExpenditure();
                default:
                    throw new IllegalStateException("Unexpected userGoal: " + user.getUserGoal());
            }
        }

        private double calculateAmountOfCaloriesPerDayWhenReducting(User user) {
            double differenceBetweenBEEAndTEE = user.getTotalEnergyExpenditure() - user.getBasalEnergyExpenditure();

            double caloriesDeficit = (differenceBetweenBEEAndTEE * 0.75) <= 500 ? (differenceBetweenBEEAndTEE * 0.75) : 500;

            return (user.getTotalEnergyExpenditure() - caloriesDeficit);
        }

        private double calculateAmountOfCaloriesPerDayWhenCarrying(User user) {
            double differenceBetweenBEEAndTEE = user.getTotalEnergyExpenditure() - user.getBasalEnergyExpenditure();

            double caloriesDeficit = (differenceBetweenBEEAndTEE * 0.75) <= 500 ? (differenceBetweenBEEAndTEE * 0.75) : 500;

            return (user.getTotalEnergyExpenditure() + caloriesDeficit);
        }
    }

    @Override
    public GenerateDietStrategyName getStrategy() {
        return GenerateDietStrategyName.HEALTH_YOUNG_HUMAN_DIET_GENERATE_STRATEGY;
    }
}
