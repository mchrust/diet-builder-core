package com.dietbuilder.stategies.impl;

import com.dietbuilder.documents.user.Gender;
import com.dietbuilder.documents.user.User;
import com.dietbuilder.documents.user.UserGoal;
import com.dietbuilder.repositories.DietRepository;
import com.dietbuilder.repositories.MealRepository;
import com.dietbuilder.services.SequenceGeneratorService;
import com.dietbuilder.strategies.impl.HealthYoungHumanDietGenerateStrategy;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Arrays;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@SpringBootTest(classes = {HealthYoungHumanDietGenerateStrategy.class})
class HealthYoungHumanDietGenerateStrategyTest {

    @Autowired
    private HealthYoungHumanDietGenerateStrategy healthYoungHumanDietGenerateStrategy;

    @MockBean
    private MealRepository mealRepository;

    @MockBean
    private DietRepository dietRepository;

    @MockBean
    private SequenceGeneratorService sequenceGeneratorService;

    @Test
    void givenSampleUser_whenProcessed_thenNoExceptionIsThrownAndRepositoriesAreCalled() {
        this.healthYoungHumanDietGenerateStrategy.generateDiet(sampleUser());
        verify(this.sequenceGeneratorService, times(1)).generateSequence(any());
        verify(this.dietRepository, times(2)).save(any());
        verify(this.mealRepository, times(0)).saveAll(any());
        verify(this.mealRepository, times(5)).findMealsByCaloriesAndMealTypeExcludingUnwantedIngredients(any(Double.class), any(Double.class), any(String.class), any(Long[].class));
    }

    private User sampleUser() {
        return User.builder()
                .userId(123L)
                .gender(Gender.MALE)
                .height(1.75)
                .weight(70.0)
                .age(30)
                .psychicalActivityRatio(1.6)
                .userGoal(UserGoal.REDUCE)
                .numberOfMeals(5)
                .unwantedComestibleProductsIDs(Arrays.asList(123L, 234L, 345L, 456L))
                .basalEnergyExpenditure(1507.98)
                .totalEnergyExpenditure(2412.77)
                .build();
    }
}
