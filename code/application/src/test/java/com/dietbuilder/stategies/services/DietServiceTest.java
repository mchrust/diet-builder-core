package com.dietbuilder.stategies.services;

import com.dietbuilder.documents.diet.Diet;
import com.dietbuilder.documents.diet.DietStatus;
import com.dietbuilder.documents.user.Gender;
import com.dietbuilder.documents.user.User;
import com.dietbuilder.documents.user.UserGoal;
import com.dietbuilder.repositories.DietRepository;
import com.dietbuilder.repositories.MealRepository;
import com.dietbuilder.services.DietService;
import com.dietbuilder.services.SequenceGeneratorService;
import com.dietbuilder.strategies.GenerateDietStrategy;
import com.dietbuilder.strategies.UserIndicatorsGenerateStrategy;
import com.dietbuilder.strategies.impl.HarrisBenedictUserIndicatorsGenerateStrategy;
import com.dietbuilder.strategies.impl.HealthYoungHumanDietGenerateStrategy;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.List;
import java.util.Objects;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(classes = {DietService.class, HarrisBenedictUserIndicatorsGenerateStrategy.class, HealthYoungHumanDietGenerateStrategy.class})
@MockBean(classes = {DietRepository.class, MealRepository.class, SequenceGeneratorService.class})
class DietServiceTest {
    
    @Autowired
    private DietService dietService;

    @ParameterizedTest
    @MethodSource("sampleUser")
    void givenSampleUser_whenGeneratingDiet_thenReturnsGeneratedDiet(User user) {
        Diet diet = this.dietService.createDiet(user);

        assertThat(diet.getDietStatus().equals(DietStatus.ACTIVE));
        assertThat(Objects.nonNull(diet.getFirstMealsList()));
        assertThat(Objects.nonNull(diet.getFifthMealsList()));
    }

    private static Stream<Arguments> sampleUser() {
        User user = User.builder()
                .userId(123L)
                .gender(Gender.MALE)
                .height(1.75)
                .weight(70.0)
                .age(30)
                .psychicalActivityRatio(1.6)
                .userGoal(UserGoal.REDUCE)
                .numberOfMeals(5)
                .unwantedComestibleProductsIDs(List.of(21L,2L))
                .build();

        return Stream.of(Arguments.of(user));
    } 
}
